import Vue from 'vue'
import VueRouter from 'vue-router'
import CampusYueqing from '../views/CampusYueqing.vue'
import CampusChashan from  '../views/CampusChashan.vue'
import CampusBinhai from  '../views/CampusBinhai.vue'
import StudentSystem from  '../views/StudentSystem.vue'
import login from  '../views/login.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component: CampusBinhai
  },
  {
    path: '/binhai',
    name: 'binhai',
    component: CampusBinhai
  },

  {
    path: '/chashan',
    name: 'chashan',
    component: CampusChashan
  },
  {
    path: '/yueqing',
    name: 'yueqing',
    component: CampusYueqing
  },
  {
    path: '/student',
    name: 'student',
    component: StudentSystem
   
  },
  {
    path: '/login',
    name: 'login',
    component: login
   
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router;
